
import mysql from 'mysql';
import EnvironmentDev from '../enviroment_dev.mjs';

var env_dev = new EnvironmentDev();

var db_config = mysql.createConnection({
  host: env_dev.database_host,
  user: env_dev.database_user,
  password: env_dev.database_password,
  database: env_dev.database_name
});

db_config.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  
});

export default db_config;