import express from 'express'; 
import SapInvoice from './OMS/model/sap_invoice.js';
import LpnReturns from './OMS/model/lpn_returns.js';
import Event from './OMS/model/Event.mjs';
import Allocation_detail from './OMS/model/Allocation/allocation.js';
import Tibco from './OMS/model/Tibco/tibco.js';
import axios from 'axios';
const app= express();

app.use(express.json({extended: false})); 

app.get("/", function(req,res){
  res.send("Welcome to the world of science fiction, conflicting theories, fantasies and some eccentric nerds!")
});

app.listen(3000, function(){
        console.log("SERVER STARTED ON localhost:3000");     
})

app.get("/getEvent",function(req,res){
    try{
        let dataBody=req.body
        let event_obj = new Event(dataBody) 
        //acknowledge_data api
        var id = null
        
        try{
            console.log(JSON.stringify(dataBody))
            let objj = JSON.stringify(dataBody)
            const obj = new SapInvoice(null,objj,new Date(),true,null,null,new Date(),true,true,null,null,null, new Date(),true,null,null,null,new Date(),null);
            
            obj.insertRecord(obj, function(err, res) {
                if (err)
                {res.send(err)};
                console.log(res)
                obj.id = res
                event_obj.process_start_sap(obj)  
      
              });
        
              
            
            res.send("200")
         
        } catch (err){
            console.log(err);
            res.send("500")
            
        }
        
        
    } catch (err){
        console.log(err);
        res.send("500")
    }
});


app.get("/test-sap",function(req,res){
    try{
        const obj = new SapInvoice(null,"test2",new Date(),true,null,null,new Date(),true,true,null,null,null, new Date(),true,null,null,null,new Date(),null);

        obj.insertRecord(obj, function(err, res) {
            if (err)
            res.send(err);
         
          });
          
        res.send("200")
    } catch (err){
        console.log(err);
        res.send("500")
    }
});


app.get("/test-lpn",function(req,res){
    var allocation_obj = []
   
    try{
        let tibco_obj = new Tibco();
        const lpn_obj = new Allocation_detail();
        var allocation_obj = []
        req.body.forEach(element => {
            allocation_obj.push(new Allocation_detail(element))
            
        })
        try{
           
            const obj_alloc = new LpnReturns(null,JSON.stringify(allocation_obj),new Date(),true,null,null,new Date(),true,true,null,null,null, new Date(),true,null,null,null,new Date(),null)
            obj_alloc.insertRecord(obj_alloc, function(err, res) {
                console.log(res)
                obj_alloc.id = res
                lpn_obj.process_start_lpn(obj_alloc)  
              });
        } catch (err){
            console.log(err);
            res.send("500")
            
        }          
        res.send("200")
    } catch (err){
        console.log(err);
        res.send("500")
    }
});

