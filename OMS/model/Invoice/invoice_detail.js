import LineItemDetail from './lineItemDetail.js';
import PaymentDetails from './payment_detail.js'
import Customer from './customer_detail.js'
import Attributes from './attribute_detail.js'
export default class Invoice_detail {
  
    constructor(obj) {
      this.invoiceId = obj.invoiceId;
      this.invoiceStatus = obj.invoiceStatus;
      this.invoiceType = obj.invoiceType;
      this.storeId = obj.storeId;
      this.registerTransNumber = obj.registerTransNumber;
      this.creationDate = obj.creationDate;
      this.issueDate = obj.issueDate;
      this.totalAmount = obj.totalAmount;
      this.totalTaxAmount = obj.totalTaxAmount;
      this.lineItemDetail = []
      obj.lineItemDetail.forEach(element => {
        this.lineItemDetail.push(new LineItemDetail(element))
      })
      this.paymentDetails = []
      obj.paymentDetails.forEach((payment) => {
        this.paymentDetails.push(new PaymentDetails(payment))
      })
      this.customerDetail = new Customer(obj.customer)
      this.attributesDetail = new Attributes(obj.attributes)
      
      
    }
  

    static getInviceJson(){
    
      let invoice_resp = new Invoice_detail({
        "invoiceId": "317289420",
        "invoiceStatus": "ISSUED",
        "invoiceType": "SHIPPING",
        "storeId": "831",
        "registerTransNumber": "22470903",
        "creationDate": "2022-01-27T16:15:58.000-05:00",
        "issueDate": "2021-12-23T23:44:21.824-05:00",
        "totalAmount": 538.04,
        "totalTaxAmount": 45.04,
        "lineItemDetail": [{
          "lineItemId": 6,
          "sku": "White-Gray-Shirt",
          "itemId": "203",
          "quantity": 1,
          "basePrice": 171.0,
          "salePrice": 180.0,
          "lineItemDateTime": "2021 - 11 - 27 T23: 38: 50.711 - 05: 00",
          "locationStoreId": 831,
          "returnExpiryDays": 60,
          "discount": {
            "discountAmount": 26.50,
            "discounts": [{
              "promoId": "fza065tdr787",
              "promoCode": "BOGUSCODE",
              "promoTitle": "ORDER DISCOUNT - PERCENT OFF",
              "value": 5.0,
              "type": "PROMOTION",
              "quantity": 1
            }]
          },
          "taxDetails": {
            "taxCode": "STATE",
            "taxAmount": 31.46,
            "taxableAmount": 496.50
          }
        }],
        "paymentDetails": [{
          "lineItemId": 2,
          "paymentType": 50,
          "amount": 538.04,
          "paymentAmount": 538.04,
          "paymentDateTime": "2021-12-23T23:44:22.253-05:00",
          "cardDetail": {
            "cardNumber": "************7541",
            "cardNumberEncrypted": "4388544146657541",
            "expirationDate": "2025-04-30T23:59:59.000-04:00",
            "paymentGateway": "CNP"
          },
          "paymentAuthorization": {
            "authorizationDateTime": "2021-12-24T04:44:22.000-05:00",
            "authorizationApprovalCode": 57,
            "authorizationNumber": "02677D",
            "expirationDate": "2025-04-30T23:59:59.000-04:00",
            "authorizationAmount": "569.94"
          }
        }],
        "customer": {
          "customerId": "3234415797",
          "customerName": "STORSETH",
          "firstName": "NICOLE",
          "suffix": "String",
          "address": {
            "line1": "3112 Belinda St",
            "line2": "",
            "city": "Seattle",
            "state": "WA",
            "postalCode": "321024",
            "country": "USA"
          },
          "phone": "(940) 704-2667",
          "email": "icolestorseth@gmail.com"
        },
        "attributes": {
          "scanType": "UPC",
          "PLUID": "194302095908",
          "MdseXrefNo": "570320538",
          "storeHierarchyId": "7723513",
          "deliveryCode": "444853150"
        }
      })
      let resp = [invoice_resp.invoiceId,invoice_resp.invoiceStatus,invoice_resp.invoiceType]
      return resp
    }
  }

