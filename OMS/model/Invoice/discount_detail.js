import Discounts from './discounts_detail.js';
export default class Discount {
  
    constructor(obj)
    {
        this.discountAmount = obj.discountAmount;
        this.discounts = [];
        obj.discounts.forEach((discount) => 
        this.discounts.push(new Discounts(discount)))

      }
    }

