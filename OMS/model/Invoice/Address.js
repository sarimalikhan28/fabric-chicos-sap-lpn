export default class Address {
  
    constructor(obj)
    {
        this.line1  = obj.line1;
        this.line2 = obj.line2;
        this.city = obj.city;
        this.state = obj.state;
        this.postalCode = obj.postalCode;
        this.country = obj.country;
        
      }
    }

