export default class CardDetail {
  
    constructor(obj)
    {
        this.cardNumber = obj.cardNumber;
        this.cardNumberEncrypted = obj.cardNumberEncrypted;
        this.expirationDate = obj.expirationDate;
        this.paymentGateway = obj.paymentGateway;
        
      }
    }

