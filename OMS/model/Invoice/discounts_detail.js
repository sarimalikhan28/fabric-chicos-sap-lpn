export default class Discounts {
  
    constructor(obj)
    {
       this.promoId = obj.promoId;
       this.promoCode = obj.promoCode;
       this.promoTitle = obj.promoTitle;
       this.value = obj.value;
       this.type = obj.type;
       this.quantity = obj.quantity;

      }
    }

