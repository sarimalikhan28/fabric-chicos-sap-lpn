import Address from "./Address.js";
export default class Customer {
    constructor(obj){
      this.customerId = obj.customerId;
      this.customerName = obj.customerName;
      this.firstName = obj.firstName;
      this.suffix = obj.suffix;
      this.address = new Address(obj.address)
      this.phone = obj.phone;
      this.email = obj.email;

    }
}