export default class PaymentAuth {
  
    constructor(obj)
    {
        this.authorizationDateTime = obj.authorizationDateTime;
        this.authorizationApprovalCode = obj.authorizationApprovalCode;
        this.authorizationNumber = obj.authorizationNumber;
        this.expirationDate = obj.expirationDate;
        this.authorizationAmount = obj.authorizationAmount;
      }
    }
