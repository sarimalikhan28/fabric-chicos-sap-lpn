import CardDetail from './card_detail.js'
import PaymentAuth from './payment_auth.js'
export default class PaymentDetails {
  
    constructor(obj)
    {
       this.lineItemId = obj.lineItemId;
       this.paymentType = obj.paymentType;
       this.amount = obj.amount;
       this.paymentAmount = obj.paymentAmount;
       this.paymentDateTime = obj.paymentDateTime;
       this.cardDetail = new CardDetail(obj.cardDetail)
       this.paymentAuthorization = new PaymentAuth(obj.paymentAuthorization)

      }
    }
