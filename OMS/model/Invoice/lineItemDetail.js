import Discount from './discount_detail.js';
import TaxDetail from './tax_details.js'

export default class LineItemDetail {
  
    constructor(obj){
      this.lineItemId = obj.lineItemId;
      this.sku = obj.sku;
      this.itemId = obj.itemId;
      this.quantity = obj.quantity;
      this.basePrice = obj.basePrice;
      this.salePrice = obj.salePrice;
      this.lineItemDateTime = obj.lineItemDateTime;
      this.locationStoreId = obj.locationStoreId;
      this.returnExpiryDays = obj.returnExpiryDays;
      this.discount = new Discount(obj.discount)
      
     
      this.taxDetails = new TaxDetail(obj.taxDetails)
    

      }
    }

