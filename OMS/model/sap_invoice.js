
import db_config from '../../database/database.js'
import Common from '../model/common.js';

var common = new Common();

export default class SapInvoice {
  
    constructor(id,eventPayload,eventReceivedAt,isEventDeserialized,orderInvoiceDetailEndpoint
        ,orderInvoiceDetailResponse,orderInvoiceDetailResponseReceivedAt,isOrderInvoiceDetailResponseDeserialized,
        isTransformedForSap,sapEndpoint,sapRequestPayload,sapResponse,sapResponseReceivedAt,isSapResponseDeserialized,
        orderAttributeEndpoint,orderAttributeRequestPayload,orderAttributeResponse,orderAttributeResponseReceivedAt,error)
        {
       this.id=id
       this.event_payload=eventPayload
       this.event_received_at=eventReceivedAt
       this.is_event_deserialized=isEventDeserialized
       this.order_invoice_detail_endpoint=orderInvoiceDetailEndpoint
       this.order_invoice_detail_response=orderInvoiceDetailResponse
       this.order_invoice_detail_response_received_at=orderInvoiceDetailResponseReceivedAt
       this.is_order_invoice_detail_response_deserialized=isOrderInvoiceDetailResponseDeserialized
       this.is_transformed_for_sap=isTransformedForSap
       this.sap_endpoint=sapEndpoint
       this.sap_request_payload=sapRequestPayload
       this.sap_response=sapResponse
       this.sap_response_received_at=sapResponseReceivedAt
       this.is_sap_response_deserialized=isSapResponseDeserialized
       this.order_attribute_endpoint=orderAttributeEndpoint
       this.order_attribute_request_payload=orderAttributeRequestPayload
       this.order_attribute_response=orderAttributeResponse
       this.order_attribute_response_received_at=orderAttributeResponseReceivedAt
       this.error=error
       this.created_date= new Date();
       this.updated_date= new Date();
      }

      /* Insert Record */
        insertRecord(newObj, result) {
            var insertId = null;
            try {
            db_config.query("INSERT INTO `chichosintegration`.`sap_invoice` set ?", newObj, function (err, res) {
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    insertId = res.insertId
                    result(null, insertId);
                    return insertId
              
                }
                });
            } catch (error) {
                console.log(common.connectError)
            }
            
            
            
           
        };

/* Get By Id */

        getById(id, result) {
        try {
            db_config.query("Select * from `chichosintegration`.`sap_invoice` where id = ? ", id, function (err, res) {
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
                }
                });
        } catch (error) {
            console.log(common.connectError)
        }
        
        };

/* Get ALL */

getAll(result) {
  try {
      db_config.query("Select * from `chichosintegration`.`sap_invoice`", function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }
          else{
            console.log('sap invoice : ', res);
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
 
};

/* Update invoice detail */
updateRecord(id, newObj, result){
  try {
      db_config.query("UPDATE `chichosintegration`.`sap_invoice` SET order_invoice_detail_endpoint=?,order_invoice_detail_response=?,order_invoice_detail_response_received_at=?,is_order_invoice_detail_response_deserialized=?,is_transformed_for_sap=? WHERE id = ?", [newObj.order_invoice_detail_endpoint,newObj.order_invoice_detail_response,newObj.order_invoice_detail_response_received_at,newObj.is_order_invoice_detail_response_deserialized,newObj.is_transformed_for_sap,id], function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }else{
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
  
};

/* Delete by Id */
deleteById(id, result){
  try {
      db_config.query("DELETE FROM `chichosintegration`.`sap_invoice` WHERE id = ?", [id], function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }
          else{
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
 
};


}



