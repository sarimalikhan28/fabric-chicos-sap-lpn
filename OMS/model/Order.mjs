import Fees from "./Order/Fees.mjs";
import Promotion from "./Order/Promotion.mjs";
import Customer from "./Order/Customer.mjs";
import Payment from "./Order/Payment.mjs";
import Attributes from "./Order/Attributes.mjs";
import Item from './Order/Item.mjs';
import ShippingInformation from './Order/ShippingInformation.mjs';
import ItemAdjustment from './Order/ItemAdjustment.mjs';
import OrderAdjustment from './Order/OrderAdjustment.mjs';

class Order {
  constructor(args) {
    this.channel = args.channel;
    this.orderNumber = args.orderNumber;
    this.type = args.type;
    this.orderDate = args.orderDate;
    this.locationNum = args.locationNum;
    this.associateId = args.associateId;
    this.orderSubTotal = args.orderSubTotal;
    this.orderDiscount = args.orderDiscount;
    this.orderTotal = args.orderTotal;
    this.taxTotal = args.taxTotal;
    this.fees = [];
    args.fees.forEach((fees) => {
      this.fees.push(new Fees(fees));
    });
    this.currency = args.currency;
    this.statusCode = args.statusCode;
    this.statusDescription = args.statusDescription;
    this.attributes = new Attributes(args.attributes);
    this.promotions = [];
    args.promotions.forEach((promotion) => {
      this.promotions.push(new Promotion(promotion));
    });
    this.customer = [];
    args.customer.forEach(customer => {
      this.customer.push( new Customer(customer));
    });
    this.payments = [];
    args.payments.forEach((payment) => {
      this.payments.push(new Payment(payment));
    });
    this.items = [];
    args.items.forEach(item => {
      this.items.push(new Item(item));
    });
    this.shippingInformation = [];
    args.shippingInformation.forEach(shippingInformation => {
      this.shippingInformation.push(new ShippingInformation(shippingInformation));
    });
    this.itemAdjustments = [];
    args.itemAdjustments.forEach(itemAdjustment => {
      this.itemAdjustments.push(new ItemAdjustment(itemAdjustment));
    })
    this.orderAdjustments = [];
    args.orderAdjustments.forEach(orderAdjustment => {
      this.orderAdjustments.push(new OrderAdjustment(orderAdjustment));
    })
  }

  get_Id() {
    return this._id || "";
  }

  get_shippingInformation() {
    return this.shippingInformation || "";
  }
  get_channel() {
    return this.channel || "";
  }
  get_orderNumber() {
    return this.orderNumber || "";
  }
  get_type() {
    return this.type || "";
  }
  get_orderDate() {
    return this.orderDate || "";
  }
  get_locationNum() {
    return this.locationNum || "";
  }
  get_associateId() {
    return this.associateId || "";
  }
  get_orderSubTotal() {
    return this.orderSubTotal || "";
  }
  get_orderDiscount() {
    return this.orderDiscount || "";
  }
  get_orderTotal() {
    return this.orderTotal || "";
  }
  get_taxTotal() {
    return this.taxTotal || "";
  }
  get_fees() {
    return this.fees || "";
  }
  get_currency() {
    return this.currency || "";
  }
  get_statusCode() {
    return this.statusCode || "";
  }
  get_statusDescription() {
    return this.statusDescription || "";
  }
  get_attributes() {
    return this.attributes || "";
  }
  get_promotions() {
    return this.promotions || "";
  }
  get_customer() {
    return this.customer || "";
  }
  get_payments() {
    return this.payments || "";
  }
  get_items() {
    return this.items || "";
  }
  get_itemAdjustments() {
    return this.itemAdjustments || "";
  }
  get_orderAdjustments() {
    return this.orderAdjustments || "";
  }

}


export default Order;