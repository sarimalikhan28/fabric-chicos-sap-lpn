import db_config from '../../database/database.js'
import Common from '../model/common.js';

var common = new Common();

export default class LpnReturns {
  
    constructor(id,eventPayload,eventReceivedAt,isEventDeserialized,orderlpnDetailEndpoint
        ,orderlpnDetailResponse,orderlpnDetailResponseReceivedAt,isOrderlpnDetailResponseDeserialized,
        isTransformedForlpn,lpnEndpoint,lpnRequestPayload,lpnResponse,lpnResponseReceivedAt,islpnResponseDeserialized,
        orderAttributeEndpoint,orderAttributeRequestPayload,orderAttributeResponse,orderAttributeResponseReceivedAt,error)
        {
       this.id=id
       this.event_payload=eventPayload
       this.event_received_at=eventReceivedAt
       this.is_event_deserialized=isEventDeserialized
       this.order_lpn_detail_endpoint=orderlpnDetailEndpoint
       this.order_lpn_detail_response=orderlpnDetailResponse
       this.order_lpn_detail_response_received_at=orderlpnDetailResponseReceivedAt
       this.is_order_lpn_detail_response_deserialized=isOrderlpnDetailResponseDeserialized
       this.is_transformed_for_lpn=isTransformedForlpn
       this.lpn_endpoint=lpnEndpoint
       this.lpn_request_payload=lpnRequestPayload
       this.lpn_response=lpnResponse
       this.lpn_response_received_at=lpnResponseReceivedAt
       this.is_lpn_response_deserialized=islpnResponseDeserialized
       this.order_attribute_endpoint=orderAttributeEndpoint
       this.order_attribute_request_payload=orderAttributeRequestPayload
       this.order_attribute_response=orderAttributeResponse
       this.order_attribute_response_received_at=orderAttributeResponseReceivedAt
       this.error=error
       this.created_date= new Date();
       this.updated_date= new Date();
       
       
      }

      /* Insert Record */
        insertRecord(newObj, result) {
            var insertId = null;
            try {
            db_config.query("INSERT INTO `chichosintegration`.`lpn` set ?", newObj, function (err, res) {
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    insertId = res.insertId
                
    
                    result(null, insertId);
                    return insertId
                
                }
                });
            } catch (error) {
                console.log(common.connectError)
            }
            
            
            
           
        };

/* Get By Id */

        getById(id, result) {
        try {
            db_config.query("Select * from `chichosintegration`.`lpn` where id = ? ", id, function (err, res) {
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
                }
                });
        } catch (error) {
            console.log(common.connectError)
        }
        
        };

/* Get ALL */

getAll(result) {
  try {
      db_config.query("Select * from `chichosintegration`.`lpn`", function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }
          else{
            console.log('lpn lpn : ', res);
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
 
};

/* Update lpn detail */
updateRecord(id, newObj, result){
  try {
      db_config.query("UPDATE `chichosintegration`.`lpn` SET order_lpn_detail_endpoint=?,order_lpn_detail_response=?,order_lpn_detail_response_received_at=?,is_order_lpn_detail_response_deserialized=?,is_transformed_for_lpn=? WHERE id = ?", [newObj.order_lpn_detail_endpoint,newObj.order_lpn_detail_response,newObj.order_lpn_detail_response_received_at,newObj.is_order_lpn_detail_response_deserialized,newObj.is_transformed_for_lpn,id], function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }else{
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
  
};

/* Delete by Id */
deleteById(id, result){
  try {
      db_config.query("DELETE FROM `chichosintegration`.`lpn` WHERE id = ?", [id], function (err, res) {
          if(err) {
            console.log("error: ", err);
            result(null, err);
          }
          else{
            result(null, res);
          }
          });
  } catch (error) {
      console.log(common.connectError)
  }
 
};


}



