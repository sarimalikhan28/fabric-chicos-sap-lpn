import {Discounts} from './discounts_detail.js';
export default class Discount {
  
    constructor(discountAmount,discounts)
    {
        this.discountAmount = discountAmount;
        this.discounts = Object.assign(discounts,Discounts);

      }
    }

