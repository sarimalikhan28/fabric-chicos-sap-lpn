import Discount from './discount_detail.js';

export default class LineItemDetail {
  
    constructor(lineItemId,sku,itemId,quantity,basePrice,salePrice,lineItemDateTime,locationStoreId,returnExpiryDays,discount){
      this.lineItemId = lineItemId;
      this.sku = sku;
      this.itemId = itemId;
      this.quantity = quantity;
      this.basePrice = basePrice;
      this.salePrice = salePrice;
      this.lineItemDateTime = lineItemDateTime;
      this.locationStoreId = locationStoreId;
      this.returnExpiryDays = returnExpiryDays;
      this.discount = Object.assign(discount,Discount);
      }
    }

