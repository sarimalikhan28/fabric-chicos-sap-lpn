import Auth_Token from "../Auth/Authentication.js";
import axios from "Axios";
import xml2js from "xml2js";
export default class Tibco {
    constructor(args){
        this.invoiceId = args[0]
        this.invoiceType = args[1]
        this.invoiceStatus = args[2]
    }

    generate_token(callback){
        // Tibco Post Request.
        const auth_obj = new Auth_Token();
        auth_obj.auth_token(function(err,data){
            if(err) callback(err);
            else{
                callback(null,data);
            }
        })
    }

    send_object_to_tibco(callback){
        console.log(this.invoiceId)
        console.log(this.invoiceStatus)
        var data = JSON.stringify({
        "invoiceId": this.invoiceId,
        "invoiceType": this.invoiceType,
        "documentType": this.invoiceStatus
        });

        var config = {
        method: 'post',
        url: 'https://apidev.chicos.com/chicos-dev/invoices',
        headers: { 
            'x-api-key': 'VIwWxvNM1o6YbJ3Km8WIU4d8APpP8B0i5JLDp7aH', 
            'User-Agent': 'fabricinteg', 
            'Content-Type': 'application/json', 
            'Cookie': 'JSESSIONID=715040DF98B87F1C2F18A93698535508'
        },
        data : data
        };  

        axios(config)
        .then(function (response) {
        // console.log(JSON.stringify(response.data));
        xml2js.parseString(response.data,{mergeAttrs:true},(err, result) => {
            console.log("DATA1 : ",result);
        });
        callback(null, response.data);
        })
        .catch(function (error) {
        console.log(error);
        callback(error);
        });
         }
        }