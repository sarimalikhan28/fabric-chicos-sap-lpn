class Fees {
  constructor(args) {
    this._id = args._id;
    this.type = args.type;
    this.value = args.value;
    this.id = args.id;
  }

  get_type() {
    return this.type || "";
  }
  get_value() {
    return this.value || "";
  }
  get_id() {
    return this.id || "";
  }
}

export default Fees;