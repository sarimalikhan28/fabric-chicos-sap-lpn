import Attributes from "./ShippingInformation/Attributes.mjs";
import Address from "./ShippingInformation/Address.mjs"

class ShippingInformation {
  constructor(args) {
    this.taxCode = args.taxCode;
    this.pickup = args.pickup;
    this.attributes = new Attributes(args.attributes);
    this.shipToId = args.shipToId;
    this.shipMethod = args.shipMethod;
    this.shipToType = args.shipToType;
    this.promisedDeliveryDate = args.promisedDeliveryDate;
    this.shipToPrice = args.shipToPrice;
    this.shipToDiscount = args.shipToDiscount;
    this.address = new Address(args.address);
    this.shippingInstructions = args.shippingInstructions;
  }
}

export default ShippingInformation;