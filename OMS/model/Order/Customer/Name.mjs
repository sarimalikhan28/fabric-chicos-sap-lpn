class Name {
  constructor(first, middle, last) {
    this.first = first;
    this.middle = middle;
    this.last = last;
  }

  get_first() {
    return this.first || "";
  }
  get_middle() {
    return this.middle || "";
  }
  get_last() {
    return this.last || "";
  }
}

export default Name;