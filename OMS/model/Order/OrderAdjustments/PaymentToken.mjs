class PaymentToken {
  constructor(args) {
    this.token = args.token;
    this.type = args.type;
  }

  get_token() {
    return this.token || "";
  }

  get_type() {
    return this.type || "";
  }
}

export default PaymentToken;
