import PaymentToken from "./OrderAdjustments/PaymentToken.mjs";

class OrderAdjustment{
    constructor(args){
        this.id = args.id;
        this.paymentToken = new PaymentToken(args.paymentToken);
        this.amount = args.amount;
        this.success = args.success;
        this.response = args.response;
        this.type = args.type;
    }
}

export default OrderAdjustment;