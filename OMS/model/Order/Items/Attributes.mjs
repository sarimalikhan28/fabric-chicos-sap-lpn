class Attributes {
    constructor(args) {
      Object.keys(args).forEach(argKey => this[argKey] = args[argKey]);
    }
  }
  
  export default Attributes;
  