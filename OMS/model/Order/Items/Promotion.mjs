class Promotion {
  constructor(args) {
    this.id = args.id;
    this.code = args.code;
    this.amount = args.amount;
    this.discountType = args.discountType;
    this.description = args.description;
    this.type = args.type;
  }

  get_id() {
    return this.id || "";
  }
  get_code() {
    return this.code || "";
  }
  get_discountType() {
    return this.discountType || "";
  }
  get_amount() {
    return this.amount || "";
  }
  get_description() {
    return this.description || "";
  }
  get_type() {
    return this.type || "";
  }
}

export default Promotion