import Fees from "./Items/Fees.mjs";
import Attributes from "./Items/Attributes.mjs";
import Promotion from "./Items/Promotion.mjs";

class Item {
  constructor(args) {
    this.lineItemId = args.lineItemId;
    this.itemId = args.itemId;
    this.sku = args.sku;
    this.skuName = args.skuName;
    this.quantity = args.quantity;
    this.fees = [];
    args.fees.forEach((fees) => {
      this.fees.push(new Fees(fees));
    });
    this.price = args.price;
    this.discount = args.discount;
    this.currency = args.currency;
    this.shipToId = args.shipToId;
    this.tax = args.tax;
    this.promotions = [];
    args.promotions.forEach(promotion => {
        this.promotions.push(new Promotion(promotion));
    });
    this.total = args.total;
    this.UOM = args.UOM;
    this.backOrdered = args.backOrdered;
    this.taxCode = args.taxCode;
    this.attributes = new Attributes(args.attributes);
    this.status = args.status;
  }
}

export default Item;
