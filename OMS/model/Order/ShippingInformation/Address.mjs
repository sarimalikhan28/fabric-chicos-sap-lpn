import Name from "../Customer/Name.mjs";

class Address{
    constructor(args){
        this.name = new Name(args.name);
        this.email = args.email;
        this.phone = args.phone;
        this.addressLine1 = args.addressLine1;
        this.addressLine2 = args.addressLine2;
        this.addressLine3 = args.addressLine3;
        this.addressLine4 = args.addressLine4;
        this.city = args.city;
        this.state = args.state;
        this.country = args.country;
        this.postalCode = args.postalCode;
        this.type = args.type;
    }
}

export default Address;