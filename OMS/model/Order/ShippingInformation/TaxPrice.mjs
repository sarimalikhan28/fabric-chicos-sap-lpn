class TaxPrice{
    constructor(args){
        this.amount = args.amount;
        this.countyTax = args.countyTax;
        this.cityTax = args.cityTax;
        this.districtTax = args.districtTax;
        this.stateTax = args.stateTax;
        this.countryTax = args.countryTax;
        this.otherTax = args.otherTax;
    }
}

export default TaxPrice;