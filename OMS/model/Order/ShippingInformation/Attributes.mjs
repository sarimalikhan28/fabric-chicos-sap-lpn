import TaxPrice from './TaxPrice.mjs'

class Attributes {
    constructor(args) {
      this.taxPrice = new TaxPrice(args.taxPrice);
    }
  }
  
  export default Attributes;