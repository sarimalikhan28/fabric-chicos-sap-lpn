import PaymentToken from './ItemAdjustments/PaymentToken.mjs'
import LineItems from './ItemAdjustments/LineItems.mjs'

class ItemAdjustment{
    constructor(args){
        this.id = args.id;
        this.paymentToken = new PaymentToken(args.paymentToken);
        this.amount = args.amount;
        this.success = args.success;
        this.response = args.response;
        this.lineItems = new LineItems(args.lineItems);
        this.type = args.type;
    }
}

export default ItemAdjustment;