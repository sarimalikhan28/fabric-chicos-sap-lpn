class LineItems{
    constructor(args){
        this.lineItemId = args.lineItemId;
        this.quantity = args.quantity;
    }
}

export default LineItems;