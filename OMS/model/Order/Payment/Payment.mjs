import Attributes from "./Payment/Attributes.mjs";
import BillToAddress from "./Payment/BillToAddress.mjs";
import PaymentIdentifier from "./Payment/PaymentIdentifier.mjs";
import PaymentToken from "./Payment/PaymentToken.mjs";

class Payment {
  constructor(args) {
    this.billToAddress = new BillToAddress(args.billToAddress);
    this.shipToId = args.shipToId[0];
    this._id = args._id;
    this.paymentDate = args.paymentDate;
    this.billToId = args.billToId;
    this.paymentIdentifier = new PaymentIdentifier(args.paymentIdentifier);
    this.paymentProvider = args.paymentProvider;
    this.attributes = new Attributes(args.attributes);
    this.paymentToken = new PaymentToken(args.paymentToken);
    this.paymentType = args.paymentType;
    this.amount = args.amount;
    this.currency = args.currency;
    this.conversion = args.conversion;
    this.paymentStatus = args.paymentStatus;
    this.id = args.id;
  }

  get_billToAddress() {
    return this.billToAddress || "";
  }
  get_shipToId() {
    return this.shipToId || "";
  }
  get_paymentDate() {
    return this.paymentDate || "";
  }
  get_billToId() {
    return this.billToId || "";
  }
  get_paymentIdentifier() {
    return this.paymentIdentifier || "";
  }
  get_paymentProvider() {
    return this.paymentProvider || "";
  }
  get_attributes() {
    return this.attributes || "";
  }
  get_paymentToken() {
    return this.paymentToken || "";
  }
  get_paymentType() {
    return this.paymentType || "";
  }
  get_amount() {
    return this.amount || "";
  }
  get_currency() {
    return this.currency || "";
  }
  get_conversion() {
    return this.conversion || "";
  }
  get_paymentStatus() {
    return this.paymentStatus || "";
  }
  get_id() {
    return this.id || "";
  }
}

export default Payment;
