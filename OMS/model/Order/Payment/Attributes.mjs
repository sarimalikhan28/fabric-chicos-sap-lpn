class Attributes {
  constructor(args) {
    this.requestedDate = args.requestedDate;
    this.expirationMonth = args.expirationMonth;
    this.expirationYear = args.expirationYear;
    this.referenceId = args.referenceId;
    this.authCode = args.authCode;
    this.authExpirationDate = args.authExpirationDate;
    this.softDecline = args.softDecline;
    this.omniToken = args.omniToken;
    this.payerId = args.payerId;
    this.paypalToken = args.paypalToken;
  }

  get_requestedDate() {
    return this.requestedDate || "";
  }
  get_expirationMonth() {
    return this.expirationMonth || "";
  }
  get_expirationYear() {
    return this.expirationYear || "";
  }
  get_referenceId() {
    return this.referenceId || "";
  }
  get_authCode() {
    return this.authCode || "";
  }
  get_authExpirationDate() {
    return this.authExpirationDate || "";
  }
  get_softDecline() {
    return this.softDecline || "";
  }
  get_omniToken() {
    return this.omniToken || "";
  }
  get_payerId() {
    return this.payerId || "";
  }
  get_paypalToken() {
    return this.paypalToken || "";
  }
}

export default Attributes;