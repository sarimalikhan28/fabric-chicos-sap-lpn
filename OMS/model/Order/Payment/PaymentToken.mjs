class PaymentToken {
  constructor(args) {
    this.token = args.token;
  }

  get_token() {
    return this.token || "";
  }
}

export default PaymentToken;
