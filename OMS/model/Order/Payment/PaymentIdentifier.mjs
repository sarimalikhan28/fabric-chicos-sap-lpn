class PaymentIdentifier {
  constructor(args) {
    this.cardIdentifier = args.cardIdentifier;
  }

  get_cardIdentifier() {
    return this.cardIdentifier || "";
  }
}

export default PaymentIdentifier;
