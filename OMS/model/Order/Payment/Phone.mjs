class Phone{
    constructor(number, type){
        this.number = number;
        this.type = type;
    }

    get_number(){
        return this.number || "";
    }

    get_type(){
        return this.type || "";
    }
}

export default Phone;
