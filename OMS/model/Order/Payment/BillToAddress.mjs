import Phone from "./Phone.mjs";
import Name from "./Name.mjs"

class BillToAddress {
  constructor(args) {
    this.name = new Name(args.name);
    this.phone =new Phone(args.phone);
    this.email = args.email;
    this.address1 = args.address1;
    this.address2 = args.address2;
    this.address3 = args.address3;
    this.address4 = args.address4;
    this.city = args.city;
    this.state = args.state;
    this.country = args.country;
    this.postalCode = args.postalCode;
    this.type = args.type;
  }

  get_name() {
    return this.name || "";
  }
  get_phone() {
    return this.phone || "";
  }
  get_email() {
    return this.email || "";
  }
  get_address1() {
    return this.address1 || "";
  }
  get_address2() {
    return this.address2 || "";
  }
  get_address3() {
    return this.address3 || "";
  }
  get_address4() {
    return this.address4 || "";
  }
  get_city() {
    return this.city || "";
  }
  get_state() {
    return this.state || "";
  }
  get_country() {
    return this.country || "";
  }
  get_postalCode() {
    return this.postalCode || "";
  }
  get_type() {
    return this.type || "";
  }
}

export default BillToAddress;