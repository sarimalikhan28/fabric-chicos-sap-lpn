import Name from "./Customer/Name.mjs";
import Phone from "./Customer/Phone.mjs";

class Customer {
  constructor(args){
    this._id = args._id;
    this.name = args.name;
    this.userId = args.userId;
    this.accountId = args.accountId;
    this.email = args.email;
    this.phone = args.phone;
    this.id = args.id;
  }

  get_name() {
    return this.name || "";
  }

  get_userId() {
    return this.userId || "";
  }

  get_accountId() {
    return this.accountId || "";
  }

  get_email() {
    return this.email || "";
  }

  get_phone() {
    return this.phone || "";
  }

  get_id() {
    return this.id || "";
  }
}


export default Customer;