export default class Items{
    constructor(obj){
        this.orderId = obj.orderId
        this.orderNumber = obj.orderNumber
        this.orderDate = obj.orderDate
        this.orderLineId = obj.orderLineId
        this.itemId = obj.itemId
        this.sku = obj.sku
        this.channelId = obj.channelId
        this.segment = obj.segment
        this.vendorId = obj.vendorId
        this.quantity = obj.quantity
        this.UOM = obj.UOM
    }
}