import ShipAddress from './ship_address.js';
import Items from './items.js';
import Attributes from './attributes.js';
export default class Allocation_detail{
    constructor(obj){
        if (obj!=null) 
        {
            this.allocationRequestId = obj.allocationRequestId;
            this.parentAllocationId = obj.parentAllocationId;
            this.allocationCreatedAt = obj.allocationCreatedAt;
            this.type = obj.type;
            this.allocationDocNum = obj.allocationDocNum;
            this.allocationDocId = obj.allocationDocId;
            this.locationNum = obj.locationNum;
            this.locationType = obj.locationType;
            this.dropShipPONumber = obj.dropShipPONumber;
            this.dropShipVendorId = obj.dropShipVendorId;
            this.promisedDeliveryDate = obj.promisedDeliveryDate;
            this.estimatedShipDate = obj.estimatedShipDate;
            this.backorderDate = obj.backorderDate;
            this.shipmentMethod = obj.shipmentMethod;
            this.shipToId = obj.shipToId;
           
            this.shipToAddress = new ShipAddress(obj.shipToAddress);
            this.items = []
            obj.items.forEach(element => {
            this.items.push(new Items(element))
            })
            this.attributes = new Attributes(obj.attributes);
        }
       
        this.lpn_obj = null
        
    }
    process_start_lpn(lpnReturns){
      
        this.lpn_obj = lpnReturns
        console.log(this.lpn_obj)
    }
}
