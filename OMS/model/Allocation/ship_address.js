export default class ShipAddress {
  
    constructor(obj)
    {
        this.addressLine1 = obj.addressLine1
        this.addressLine2 = obj.addressLine2
        this.addressLine3 = obj.addressLine3
        this.addressLine4 = obj.addressLine4
        this.city = obj.city
        this.state = obj.state
        this.country = obj.country
        this.postalCode = obj.postalCode
        this.type = obj.type
        this.latitude = obj.latitude
        this.longitude = obj.longitude
        
      }
    }

