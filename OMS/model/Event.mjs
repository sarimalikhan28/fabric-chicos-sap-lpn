import Order from './Order.mjs';
import Invoice_detail from './Invoice/invoice_detail.js'
import Tibco from './Tibco/tibco.js'
import axios from 'axios'
class Event {

    constructor(args){
        this.eventType = args.eventType;
        this.order = new Order(args.order);
        this.sap_obj = null;
        this.lpn_obj = null;
        console.log("Here")
           // get invoice detail response by order id...
        let response = Invoice_detail.getInviceJson();
        console.log(response);
        let tibco_obj = new Tibco(response)
    
        tibco_obj.send_object_to_tibco(function(err,data){
            console.log("DATA :::  ", data);
        })
        
        tibco_obj.generate_token(function(err,data){
            if(err){return err}
            else{  
                var acknowledge_data = JSON.stringify({
                "invoiceId": "62a1a9a783f03b388921cf7f",
                "acknowledgedAt": "2022-08-02T11:52:09.332Z"
                });

                var config = {
                method: 'post',
                url: 'https://stg02.oms.fabric.inc/invoice/post/acknowledge',
                headers: { 
                    'Authorization': JSON.stringify(data), 
                    'x-site-context': '{"channel":12,"account":"5f689caa4216e7000750d1ef","date":"2022-05-18T09:05:19.647Z","stage":"stg02","site":"local"}', 
                    'Content-Type': 'application/json'
                },
                ack_data : acknowledge_data
                };

                axios(config)
                .then(function (response) {
                console.log(JSON.stringify(response.ack_data));
                })
                .catch(function (error) {
                console.log(error);
                });

                
            }
        });
    }
    process_start_sap(sapInvoice){
      
        this.sap_obj = sapInvoice
        console.log(this.ssap_obj)
    }

}

export default Event;